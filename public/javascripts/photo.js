$(function() {

    var browserHight = $(window).height();
    var browserWidth = $(window).width();
    window.onresize = function() {
        browserHight = $(window).height();
        browserWidth = $(window).width();
    }
    /*
        逻辑处理与数据请求
    */
    getPhotos();

    function getPhotos() {
        // $.ajax({
        //     url: 'getPhoto',
        //     type: "POST",
        //     datatype: "json",
        //     error: function() {
        //         alert("error occured!!!");
        //     },
        //     success: function(data) {
        //         var photosList = eval("(" + data + ")");
        //         for (var i = 0; i < photosList.length; i++) {
        //             var name = photosList[i].photoName;
        //             $('#content').append("<div class='smallphoto'><img src='images/photos/" + name + "'></div>");
        //         }
        //     }
        // })
        var route = "photo/getPhotoList";
        var args = null;
        ajaxPost(route, args, function(data) {
            for (i in data.photoList) {
                var url = data.photoList[i].photoImgFileUrl + data.photoList[i].photoImgUrl + "?imageMogr2/thumbnail/180x130!";
                $('#content').append("<div class='smallphoto'><img src='" + url + "'></div>");
            }
        })
    }

    /*
        控制跳转
    */

    /*
        页面特效
    */

    /*小图片*/
    var isOpen = false; //判断图片是否已经开启大图
    var isOpenFinish = false; //判断大图是否已经打开
    var initX, initY; //获取小图片初始位置
    var currentphotoId; //操作当前图片
    var currentphotoIndex; //当前图片的index
    var photoCount; //当前页的小图片数量

    $(".smallphoto").live('click', function(e) {
        currentphotoId = $(this);
        photoCount = $("#content").children(".smallphoto").length;
        if (!isOpen) {
            currentphotoIndex = currentphotoId.index();
            isOpen = true;
            $("body").css("background-color", "#A0A0A0");
            $(".smallphoto").css("opacity", "0.3");
            $("#header").css("opacity", "0.3");
            $("#nav").css("opacity", "0.3");
            $("#page").css("opacity", "0.3");
            var photo = $(this).find("img").attr("src");
            var photo = photo.substring(0,photo.indexOf("?"));
            $("body").append("<div id='movephoto'><img src='" + photo + "'><div id='prev'></div><div id='next'></div></div>");
            var offset = $(this).offset(); //DIV在页面的位置  
            _x = e.pageX - offset.left; //获得鼠标指针离DIV元素左边界的距离  
            _y = e.pageY - offset.top; //获得鼠标指针离DIV元素上边界的距离 
            var st = $(document).scrollTop();
            initX = e.pageX - _x;
            initY = e.pageY - _y - st;
            $("#movephoto").css("left", initX);
            $("#movephoto").css("top", initY);

            $("#movephoto").animate({
                'left': browserWidth / 2 - 350,
                'top': browserHight / 2 - 250,
                'width': 700,
                'height': 500
            }, 300, function() {
                isOpenFinish = true;
                $("#movephoto").animate({
                    'height': 500
                });
                $("#movephoto").append("<div id='close'></div>");
            });

        } else {
            // alert('图片已经打开了');
        }
    })
    $('#movephoto').live('mouseover', function() {
        if (isOpenFinish) {
            $("#prev").show();
            $("#next").show();
        }
    })

    $("#next").live('click', function() {
        if (currentphotoIndex < photoCount - 1) {
            currentphotoId = currentphotoId.next();
            cPhoto = currentphotoId.find("img").attr("src");
            cPhoto = cPhoto.substring(0,cPhoto.indexOf("?"));
            $("#movephoto img").attr('src', cPhoto);
            currentphotoIndex++;
        } else {
            alert("图片已经是最后一页");
        }

    })
    $("#prev").live('click', function() {
        if (currentphotoIndex > 0) {
            currentphotoIndex--;
            currentphotoId = currentphotoId.prev();
            cPhoto = currentphotoId.find("img").attr("src");
            cPhoto = cPhoto.substring(0,cPhoto.indexOf("?"));
            $("#movephoto img").attr('src', cPhoto);
        } else {
            alert("图片已经是首页");
        }

    })
    $('#movephoto').live('mouseleave', function() {
        $('#prev').hide();
        $('#next').hide();
    })
    $('#close').live('mouseover', function() {
        $("#close").css('background-position', '0 0');
    })
    $('#close').live('mouseleave', function() {
        $("#close").css('background-position', '0 48px');
    })
    $('#close').live('click', function() {
        $("body").css("background-color", "#E0E0E0");
        $(".smallphoto").css("opacity", "1");
        $("#header").css("opacity", "1");
        $("#nav").css("opacity", "1");
        $("#page").css("opacity", "1");
        isOpen = false;
        $("#movephoto").animate({
            'left': initX,
            'top': initY,
            'width': 180,
            'height': 130
        }, 300, function() {
            isOpenFinish = false;
            $("#movephoto").remove();
        });
    })
})