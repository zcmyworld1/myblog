$(function() {
	getMessageList();
	$(window).scroll(function() {
		if ($(document).scrollTop() >= 300) {
			$("#toTop").show();
		} else {
			$("#toTop").hide();
		}
	})
	$("#toTop").click(function() {
		toTop();
	})
	var messageCount;
	var currentMessage;
	var colorClass;
	var currentMessageClass;
	var currentMessageContentClass;
	$("#openMessageBox").click(function() {
		$("#wrap").css("opacity", "0.6");
		$("#sendMessageBox").slideDown(100);
		messageCount = $("#messageList").children().length;
		if (messageCount) {
			currentMessage = $("#messageList").children().last().attr("class").match(/[1-9][0-9]*/);
			if (currentMessage == 15) {
				currentMessage = 1;
			} else {
				currentMessage = parseInt(currentMessage) + 1;
			}
		} else {
			currentMessage = 1;
		}
		colorClass = "m" + currentMessage + "_" + "color";
		currentMessageClass = "message_" + currentMessage;
		currentMessageContentClass = "m" + currentMessage + "_" + "content";
		$("#sendMessageBox_content").attr("class", colorClass);
		$("#sendMessageBox_content").focus();
	})
	$("#sendMessageBox_ensure").click(function() {
		$("#sendMessageBox").hide();
		$("#toTop").show();
		$("#wrap").css("opacity", "1");
		var content = $("#sendMessageBox_content").val();
		if(content.length == 0){
			alert("输入不能为空");
			return;
		}
		var attack = content.indexOf("<");
		if (attack > -1) {
			content = content.replace(/</g,"&#60");
		};
		var attack = content.indexOf(">");
		if (attack > -1) {
			content = content.replace(/>/g,"&#62");
		};
		var html = "<div class='" + currentMessageClass + "'>" +
			"<div class='" + currentMessageContentClass + "'>" +
			content +
			"</div>" +
			"</div>"
		$("#messageList").append(html);
		t = $("#messageList").children().last().offset().top;
		if (navigator.userAgent.indexOf('Firefox') >= 0) {
			document.documentElement.scrollTop = t;
		} else {
			$("body").animate({
				scrollTop: t
			}, 300)
		}
		var route = "message/messageAdd";
		var args = {
				content: content
			}
			ajaxPost(route, args, function(data) {
				if (data.error) {
					alert("系统错误");
				}
			})
	})
	$("#sendMessageBox_close").click(function() {
		$("#sendMessageBox").hide();
		$("#wrap").css("opacity", "1");
	})
})


function toTop() {
	var t = 0;
	$("body,html").animate({
		scrollTop: t
	}, 300)
}

function getMessageList() {
	var route = "message/getMessageList";
	var args = null;
	ajaxPost(route, args, function(data) {
		var messageNumPage = Math.ceil(data.messageList.length / 15);
		var currentMessage = 0;
		for (i in data.messageList) {
			console.log(currentMessage)
			if (currentMessage == 15) {
				currentMessage = 1;
				colorClass = "m" + currentMessage + "_" + "color";
				currentMessageClass = "message_" + currentMessage;
				currentMessageContentClass = "m" + currentMessage + "_" + "content";
				var html = "<div class='" + currentMessageClass + "'>" +
					"<div class='" + currentMessageContentClass + "'>" +
					data.messageList[i].content +
					"</div>" +
					"</div>";
				$("#messageList").append(html);
			} else {
				currentMessage = parseInt(currentMessage) + 1;
				colorClass = "m" + currentMessage + "_" + "color";
				currentMessageClass = "message_" + currentMessage;
				currentMessageContentClass = "m" + currentMessage + "_" + "content";
				var html = "<div class='" + currentMessageClass + "'>" +
					"<div class='" + currentMessageContentClass + "'>" +
					data.messageList[i].content +
					"</div>" +
					"</div>";
				$("#messageList").append(html);
			}
		}
	})
}