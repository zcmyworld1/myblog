var currentPage = 1; //当前页
var currentPageIdNum = "1";
var articleCount = 0;
var lastPage = 1;
var pageSize = 10;
$(function() {

    $(".friendUrl_ele").click(function() {
        var url_name = $(this).html();
        if (url_name == '第256号程序员') {
            window.open('http://www.coder256.com/')
        }
        if (url_name == '华尔兹的后街') {
            window.open('http://sixbyte.sinaapp.com/')
        }
        if (url_name == '梦马') {
            window.open('http://doublewong.com/')
        }
    })

    //
    //获取浏览器宽高
    var browserHight = $(window).height();
    var browserWidth = $(window).width();
    $(window).scroll(function() {
        if ($(document).scrollTop() >= 300) {
            $("#toTop").show();
        } else {
            $("#toTop").hide();
        }
    })
    $("#toTop").click(function() {
        toTop();
    })

    //
    //初始化
    if ($.cookie('only') == "onlyTitle" || browserWidth < 600) {
        $("#only").html("显示摘要");
        $("#only").attr("class", "onlyMore");
        getArticleListTitle(currentPage);
    } else {
        $("#only").html("只看标题");
        $("#only").attr("class", "onlyTitle");
        getArticleList(currentPage); //默认显示摘要
    }
    getDataCount();
    getTimeHistory();
    //只看标题/摘要
    $(document).on("click", ".onlyTitle", function() { //只显示标题
        $("#only").html("显示摘要");
        $(".article").remove();
        getArticleListTitle(currentPage);
        $.cookie('only', 'onlyTitle', {
            expires: 3650
        });
        $("#only").attr("class", "onlyMore");
    });
    $(document).on("click", ".onlyMore", function() { //显示摘要
        $("#only").html("只看标题");
        $("#only").attr("class", "onlyTitle");
        $(".article").remove();
        getArticleList(currentPage);
        $.cookie('only', 'onlyMore', {
            expires: 3650
        });
    })

    //读取文章
    $(".article_readmore").live('click', function() {
        var articleId = $(this).parent().parent().find(".articleId").val();
        var readCountSpan = $(this).parent().parent().find(".article_operation").find(".article_readCount").find("span");
        $(readCountSpan).html((parseInt($(readCountSpan).html()) + 1));
        window.open('/singleArticle?articleId=' + articleId);
    })
    $(".article_title").live('click', function() {
        var articleId = $(this).parent().find(".articleId").val();
        var readCountSpan = $(this).parent().find(".article_operation").find(".article_readCount").find("span");
        $(readCountSpan).html((parseInt($(readCountSpan).html()) + 1));
        var smallReadCountSpan = $(this).parent().find(".small_article_readCount").find("span");
        $(smallReadCountSpan).html((parseInt($(smallReadCountSpan).html()) + 1));
        window.open('/singleArticle?articleId=' + articleId);
    })
    $(".article").live('click', function() {
        var articleId = $(this).find(".articleId").val();
        window.open('/singleArticle?articleId=' + articleId);
    })
    $(".article_comment").live('click', function() {
        var articleId = $(this).parent().parent().find(".articleId").val();
        var readCountSpan = $(this).parent().parent().find(".article_operation").find(".article_readCount").find("span");
        $(readCountSpan).html((parseInt($(readCountSpan).html()) + 1));
        window.open('/singleArticle?articleId=' + articleId);
    })

    function drawContent() {
            $(".article").remove();
            if ($.cookie('only') == "onlyTitle") {
                getArticleListTitle(currentPage);
            } else {
                getArticleList(currentPage); //默认显示摘要
            }
            toTop();
        }
        //分页样式
    $(document).on('mousedown', '.page_btn', function() {
            btn = $(this).html();
            if (btn == '...') {
                return;
            }
            $(this).css("box-shadow", '0px 0px 0px');
        })
        //分页算法
    $(document).on('mouseup', '.page_btn', function() {
        $(this).css("box-shadow", '5px 5px 5px #A5A5A5');
        btn = $(this).html();
        if (btn == '...') {
            return;
        }
        if (btn == '上一页') {
            if (currentPage == 1) {
                alert("已经是首页");
                return;
            }
            if (lastPage <= 9) {
                var prePageIdNum = parseInt(currentPageIdNum) - 1;
                currentPage = currentPage - 1;
                currentPageId = "#page_" + prePageIdNum;
                currentPageIdNum = prePageIdNum;
                $(".page_btn").css("background-color", "white");
                $(currentPageId).css("background-color", "rgb(3,169,220)");
                $(currentPageId).html(currentPage);
                drawContent();
                toTop();
                return;
            }
            if (currentPageIdNum == 5) {
                if (currentPage <= 5) {
                    $("#page_2").html(2)
                } else {
                    currentPage = currentPage - 1
                    $("#page_8").html('...');
                    $("#page_3").html(currentPage - 2)
                    $("#page_4").html(currentPage - 1)
                    $("#page_5").html(currentPage)
                    $("#page_6").html(currentPage + 1)
                    $("#page_7").html(currentPage + 2)
                    toTop();
                    return;
                }
            }
            var prePageIdNum = parseInt(currentPageIdNum) - 1;
            currentPage = currentPage - 1;
            currentPageId = "#page_" + prePageIdNum;
            currentPageIdNum = prePageIdNum;
            $(".page_btn").css("background-color", "white");
            $(currentPageId).css("background-color", "rgb(3,169,220)");
            $(currentPageId).html(currentPage);
            drawContent();
            toTop();
            return;
        }
        if (btn == '下一页') {
            if (currentPage == lastPage) {
                alert("已经是最后一页");
                return;
            }
            if (lastPage <= 9) {
                var nextPageIdNum = parseInt(currentPageIdNum) + 1;
                currentPage = currentPage + 1;
                currentPageId = "#page_" + nextPageIdNum;
                currentPageIdNum = nextPageIdNum;
                $(".page_btn").css("background-color", "white");
                $(currentPageId).css("background-color", "rgb(3,169,220)");
                $(currentPageId).html(currentPage);
                $(".article").remove();
                drawContent();
                toTop();
                return;
            }
            if (currentPageIdNum == 5) {
                if (currentPage >= lastPage - 4) {
                    $("#page_8").html(lastPage - 1)
                } else {
                    currentPage = currentPage + 1
                    $("#page_2").html('...');
                    $("#page_3").html(currentPage - 2)
                    $("#page_4").html(currentPage - 1)
                    $("#page_5").html(currentPage)
                    $("#page_6").html(currentPage + 1)
                    $("#page_7").html(currentPage + 2)
                    toTop();
                    return;
                }
            }
            var nextPageIdNum = parseInt(currentPageIdNum) + 1;
            currentPage = currentPage + 1;
            currentPageId = "#page_" + nextPageIdNum;
            currentPageIdNum = nextPageIdNum;
            $(".page_btn").css("background-color", "white");
            $(currentPageId).css("background-color", "rgb(3,169,220)");
            $(currentPageId).html(currentPage);
            $(".article").remove();
            drawContent();
            toTop();
            toTop();
        }
        if (btn != '上一页' && btn != '下一页') {
            currentPageId = this;
            currentPageIdNum = $(this).attr("id").match(/\d+/g);
            currentPage = parseInt(btn);
            if (lastPage <= 9) {
                $(".page_btn").css("background-color", "white");
                $(currentPageId).css("background-color", "rgb(3,169,220)");
                $(currentPageId).html(currentPage);
                drawContent();
                toTop();
                return;
            }
            if (currentPage > 4 && currentPage < lastPage - 3) {
                $("#page_2").html('...');
                $("#page_3").html(currentPage - 2)
                $("#page_4").html(currentPage - 1)
                $("#page_5").html(currentPage)
                $("#page_6").html(currentPage + 1)
                $("#page_7").html(currentPage + 2)
                $("#page_8").html('...');
                currentPageIdNum = 5;
                $(".page_btn").css("background-color", "white");
                $("#page_5").css("background-color", "rgb(3,169,220)");
                toTop();
            }
            if (currentPage == 4) {
                $("#page_2").html(2);
                $("#page_3").html(3)
                $("#page_4").html(4)
                $("#page_5").html(5)
                $("#page_6").html(6)
                $("#page_7").html(7)
                $("#page_8").html('...');
                $(".page_btn").css("background-color", "white");
                currentPageIdNum = 4;
                $('#page_4').css("background-color", "rgb(3,169,220)");
                toTop();
            }
            if (currentPage < 4) {
                $("#page_2").html(2);
                $("#page_3").html(3)
                $("#page_4").html(4)
                $("#page_5").html(5)
                $("#page_6").html(6)
                $("#page_7").html(7)
                $("#page_8").html('...');
                $(".page_btn").css("background-color", "white");
                $(currentPageId).css("background-color", "rgb(3,169,220)");
                toTop();
            }
            if (currentPage == lastPage - 3) {
                $("#page_2").html('...');
                $("#page_3").html(lastPage - 6)
                $("#page_4").html(lastPage - 5)
                $("#page_5").html(lastPage - 4)
                $("#page_6").html(lastPage - 3)
                $("#page_7").html(lastPage - 2)
                $("#page_8").html(lastPage - 1);
                $(".page_btn").css("background-color", "white");
                currentPageIdNum = 6;
                $('#page_6').css("background-color", "rgb(3,169,220)");
                toTop();
            }
            if (currentPage > lastPage - 3) {
                $("#page_2").html('...');
                $("#page_3").html(lastPage - 6)
                $("#page_4").html(lastPage - 5)
                $("#page_5").html(lastPage - 4)
                $("#page_6").html(lastPage - 3)
                $("#page_7").html(lastPage - 2)
                $("#page_8").html(lastPage - 1);
                $(".page_btn").css("background-color", "white");
                $(currentPageId).css("background-color", "rgb(3,169,220)");
                toTop();
            }
        }
        $(".article").remove();
        if ($.cookie('only') == "onlyTitle") {
            getArticleListTitle(currentPage);
        } else {
            getArticleList(currentPage); //默认显示摘要
        }
    })

    if (browser.versions.mobile) {}
    //welcome to myworld 样式
    if ($.cookie('thecookie') == "notShowWelcome" || browserWidth < 600) {
        $("#header").css('color', 'black');
        $("#blogNameLine").show();
        $("#slogan").css('color', 'black');
        $("#searchBox").show();
        $("#wrap").show();
        return;
    }
    $("#isShowWelcomeBack").click(function() {
        $("#isShowWelcome").hide();
    });
    $("#isShowWelcomeSure").click(function() {
        $.cookie('thecookie', 'notShowWelcome', {
            expires: 3650
        });
        $("#isShowWelcome").hide();
    })

    welcome();
    //设置计时器选择是否不再显示welcome样式
    setTimeout(function() {
        $("#isShowWelcome").animate({
            'margin-top': 0
        }, function() {
            setTimeout(function() {
                $("#isShowWelcomeHint").find("span").html(5);
            }, 0);
            setTimeout(function() {
                $("#isShowWelcomeHint").find("span").html(4);
            }, 1000);
            setTimeout(function() {
                $("#isShowWelcomeHint").find("span").html(3);
            }, 2000);
            setTimeout(function() {
                $("#isShowWelcomeHint").find("span").html(2);
            }, 3000);
            setTimeout(function() {
                $("#isShowWelcomeHint").find("span").html(1);
            }, 4000);
            setTimeout(function() {
                $("#isShowWelcomeTime").html(0);
                $("#isShowWelcome").animate({
                    'margin-top': -120
                }, function() {
                    $("#isShowWelcome").hide();
                });
            }, 5000);
        })
    }, 4000);
})

$("#photo").click(function() {
    window.location.href = "/photo";
})

function welcome() {
    var x = $('#slogan').offset().top;
    var y = $('#slogan').offset().left;
    $("#welcome").fadeIn(0, function() {
        $("#tomyworld").fadeIn(2500, function() {
            $("#welcomeBox").animate({
                'width': 200,
                'height': 50,
                'font-size': 15,
                "top": x + 28,
                "left": y + 240,
            }, function() {
                $("#welcomeBox").hide();
                $("#header").css('color', 'black');
                $("#blogNameLine").show();
                $("#slogan").css('color', 'black');
                $("#searchBox").show();
                $("#wrap").show();
            })
        });
    });
}
function getTimeHistory(){
    var route = 'article/getTimeHistroy';
    var args = null;
    ajaxPost(route,args,function(data){
        console.log(data)
        for(i in data.timeHistory){
            var html = "<div class='timeHistory_ele'>"+data.timeHistory[i].time+"("+data.timeHistory[i].articleCount+")</div>";
            $("#timeHistory").append(html)
        }
    })
    //<div class='timeHistory_ele'>2014年12月(1)</div>
}


function getDataCount() {
    var route = 'article/getDataCount';
    var args = null;
    ajaxPost(route, args, function(data) {
        var dataCount = data.articleCount[0].articleCount;
        articleCount = dataCount;
        var dataCount = "(" + dataCount + ")";
        $("#dataCount_data").html(dataCount);
        $("#timeHistoryCount").html(dataCount)
        initPage(articleCount);
    })
}

function initPage(articleCount) {
    // var articleCount = 150;
    lastPage = Math.ceil(articleCount / pageSize);
    //保持最多9个按钮
    if (lastPage <= 9) {
        for (var i = 0; i < lastPage; i++) {
            if (i == 0) {
                var html = "<div class='page_element page_btn' style='background-color:rgb(3,169,220)' id='page_" + (i + 1) + "'>" + (i + 1) + "</div>";
                $("#page_eleBox").append(html);
            } else {
                var html = "<div class='page_element page_btn' id='page_" + (i + 1) + "'>" + (i + 1) + "</div>";
                $("#page_eleBox").append(html);
            }
        }
    } else {
        //前7个按钮数字显示
        for (var i = 0; i < 7; i++) {
            if (i == 0) {
                var html = "<div class='page_element page_btn' style='background-color:rgb(3,169,220)' id='page_" + (i + 1) + "'>" + (i + 1) + "</div>";
                $("#page_eleBox").append(html);
            } else {
                var html = "<div class='page_element page_btn' id='page_" + (i + 1) + "'>" + (i + 1) + "</div>";
                $("#page_eleBox").append(html);
            }
        };
        //后两个按钮用省略号和最后一页显示
        var html = "<div class='page_element page_btn' id='page_8'>...</div>";
        $("#page_eleBox").append(html);
        var html = "<div class='page_element page_btn' id='page_9'>" + lastPage + "</div>";
        $("#page_eleBox").append(html);
    }
}


function getArticleList(currentPage) {
    var route = "article/getArticleList";
    var args = {
        currentPage: currentPage
    };
    ajaxPost(route, args, function(data) {
        for (var i in data.articleList) {
            if (data.articleList[i].content.length > 200) {
                data.articleList[i].content = data.articleList[i].content.substring(0, 200);
                //当<img src=">漏了后面一个"号的时候，会出bug，这段代码检车<img src="后面是否有跟随",如果没有则把img删掉
                var start = data.articleList[i].content.lastIndexOf("<img src=");
                if (start == -1) {
                    data.articleList[i].content = data.articleList[i].content + "<br>" + "......";
                } else {
                    var lastString = data.articleList[i].content.substring(start, 200);
                    if (!/src=".*"/.test(lastString)) {
                        data.articleList[i].content = data.articleList[i].content.substring(0, start);
                        data.articleList[i].content = data.articleList[i].content + "<br>" + "......";
                    } else {
                        data.articleList[i].content = data.articleList[i].content + "<br>" + "......";
                    }
                }
            }
            var html = "<div class='article'>" +
                "<input type='hidden' class='articleId' value='" + data.articleList[i].articleId + "'>" +
                "<div class='article_title'>" + data.articleList[i].title + "</div>" +
                "<div class='article_time'>" + new Date(data.articleList[i].time).toLocaleString() + "</div>";
            if (data.articleList[i].titleImg != null) {
                html = html + " <div class = 'article_photo' ><img src='" + data.articleList[i].titleImg + "'>" +
                    "</div>";
            }
            html = html +
                "<div class='article_content'>" +
                data.articleList[i].content +
                "</div>" +
                " <div class='article_operation'>" +
                "<div class='article_comment_img'>" +
                "<img src='images/sysImg/comment.png'>" +
                "</div>" +
                "<div class='article_readCount'>阅读数(<span>" + data.articleList[i].readCount + "</span>)</div>" +
                "<div class='article_comment'>评论数(<span>" + data.articleList[i].commentCount + "</span>)</div>" +
                "<div class='article_readmore'>Read More</div>" +
                "</div>" +
                "</div>";
            $("#articleBox").append(html);
        }
    })
}

function getArticleListTitle(currentPage) {
    var route = "article/getArticleList";
    var args = {
        currentPage: currentPage
    };
    ajaxPost(route, args, function(data) {
        for (var i in data.articleList) {
            if (data.articleList[i].content.length > 200) {
                data.articleList[i].content = data.articleList[i].content.substring(0, 200);
                data.articleList[i].content = data.articleList[i].content + "<br>" + "......";
            }
            var timeReady = new Date(data.articleList[i].time);
            var trueTime = timeReady.getFullYear() + "\/" + (timeReady.getMonth() + 1) + "\/" + timeReady.getDate() + " " + timeReady.getHours() + ":" + timeReady.getMinutes();
            var html = "<div class='article'>" +
                "<input type='hidden' class='articleId' value='" + data.articleList[i].articleId + "'>" +
                "<div class='article_title'>" + data.articleList[i].title + "</div>" +
                "<div class='small_article_time'>" + trueTime + "</div>" +
                "<div class='small_article_readCount'>阅读数(<span>" + data.articleList[i].readCount + "</span>)</div>" +
                "<div class='small_article_comment'>评论数(<span>" + data.articleList[i].commentCount + "</span>)</div>" +
                "</div>";
            $("#articleBox").append(html);
        }
    })
}

function toTop() {
    var t = 0;
    $("html,body").animate({
        scrollTop: t
    }, 300)
}

var browser = {
    versions: function() {
        var u = navigator.userAgent,
            app = navigator.appVersion;
        return { //移动终端浏览器版本信息
            trident: u.indexOf('Trident') > -1, //IE内核
            presto: u.indexOf('Presto') > -1, //opera内核
            webKit: u.indexOf('AppleWebKit') > -1, //苹果、谷歌内核
            gecko: u.indexOf('Gecko') > -1 && u.indexOf('KHTML') == -1, //火狐内核
            mobile: !!u.match(/AppleWebKit.*Mobile.*/), //是否为移动终端
            ios: !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/), //ios终端
            android: u.indexOf('Android') > -1 || u.indexOf('Linux') > -1, //android终端或uc浏览器
            iPhone: u.indexOf('iPhone') > -1, //是否为iPhone或者QQHD浏览器
            iPad: u.indexOf('iPad') > -1, //是否iPad
            webApp: u.indexOf('Safari') == -1, //是否web应该程序，没有头部与底部
        };
    }(),
    language: (navigator.browserLanguage || navigator.language).toLowerCase()
}