var dbclient = require('./mysql/mysql').pool;
var CommentDao = module.exports;


CommentDao.insertComment = function(commentContent, articleId, callback) {
	var sql = 'insert into comment (commentContent, articleId) values (?,?)';
	var args = [commentContent, articleId];
	dbclient.query(sql, args, function(err, res) {
		callback(err, res);
	});

}

CommentDao.getCommentList = function(articleId,callback) {
	var sql = 'select commentContent,commentTime from comment where articleId = ?';
	var args = [articleId];
	dbclient.query(sql, args, function(err, res) {
		callback(err, res);
	});
}