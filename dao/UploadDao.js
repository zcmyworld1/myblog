var UploadDao = module.exports;
var dbclient = require('./mysql/mysql').pool;


UploadDao.insertArticle = function(content, title, titleImg,callback) {
	var sql = 'insert into article (content,title,titleImg) values (?,?,?)';
	var args = [content, title, titleImg];
	dbclient.query(sql, args, function(err, res) {
		callback(err, res);
	});
}

UploadDao.updateArticle = function(content, title, articleId, callback) {
	var sql = 'update article set title = ? ,content = ? where articleId = ?';
	args = [title, content, articleId];
	dbclient.query(sql, args, function(err, res) {
		if (err !== null) {
			callback(err, null)
		} else {
			if (!!res && res.affectedRows > 0) {
				callback(err, true);
			} else {
				callback(err, false);
			}
		}
	});
}