var ArticleDao = module.exports;
var dbclient = require('./mysql/mysql').pool;
var Promise = require('promise');

ArticleDao.insertArticle = function(content, title, titleImg, articleType, readCount, callback) {
	if (titleImg) {
		var sql = 'insert into article (content,title,titleImg,articleType,readCount) values (?,?,?,?,?)';
		var args = [content, title, titleImg, articleType, readCount];
		dbclient.query(sql, args, function(err, res) {
			callback(err, res);
		});
	} else {
		var sql = 'insert into article (content,title,articleType,readCount) values (?,?,?,?)';
		var args = [content, title, articleType, readCount];
		dbclient.query(sql, args, function(err, res) {
			callback(err, res);
		});
	}
}

//md
ArticleDao.md_insertArticle = function(args, callback) {
	var sql = 'insert into article (content, md_content, title, md_content_history, content_history) values (?,?,?,?,?)';
	var args = [args.content, args.md_content, args.title, args.md_content_history, args.content_history];
	dbclient.query(sql, args, function(err, res) {
		callback(err, res);
	});
}


ArticleDao.getTimeHistroy = function(callback) {
	var sql = 'select date_format(time,\'%Y-%m\') AS time,count(*) as articleCount from article group by date_format(time, \'%Y-%m\')';
	var args = [];
	dbclient.query(sql, args, function(err, res) {
		callback(err, res);
	})
}
ArticleDao.getArticleList = function(current, pageSize, callback) {
	var sql = 'SELECT a.title,a.articleId,a.content,a.articleType,a.readCount,a.time,count(commentId) AS commentCount FROM article AS a LEFT JOIN `comment` AS c ON a.articleId = c.articleId GROUP BY articleId order by time desc limit ?,?';
	var args = [current, pageSize];
	dbclient.query(sql, args, function(err, res) {
		callback(err, res);
	});
}

ArticleDao.getDataCount = function(callback) {
	var sql = 'select count(articleId) as articleCount from article';
	var args = [];
	dbclient.query(sql, args, function(err, res) {
		callback(err, res);
	});
}

ArticleDao.getSingleArticle = function(articleId, callback) {
	var sql = 'select articleId,title,titleImg,content,time,md_content,content_history,md_content_history from article where articleId = ?'
	var args = [articleId];
	dbclient.query(sql, args, function(err, res) {
		callback(err, res);
	});
}

ArticleDao.updateArticle = function(content, title, articleId, callback) {
	var sql = 'update article set title = ? ,content = ? where articleId = ?';
	args = [title, content, articleId];
	dbclient.query(sql, args, function(err, res) {
		if (err !== null) {
			callback(err, null)
		} else {
			if (!!res && res.affectedRows > 0) {
				callback(err, true);
			} else {
				callback(err, false);
			}
		}
	});
}

ArticleDao.md_updateArticle = function(content, content_history, md_content, md_content_history, title, articleId, callback) {
	var sql = 'update article set title = ? ,content = ? ,content_history = ?, md_content = ?, md_content_history = ? where articleId = ?';
	args = [title, content, content_history, md_content, md_content_history, articleId];
	dbclient.query(sql, args, function(err, res) {
		if (err !== null) {
			callback(err, null)
		} else {
			if (!!res && res.affectedRows > 0) {
				callback(err, true);
			} else {
				callback(err, false);
			}
		}
	});
}


//promise
// ArticleDao.updateReadCount = function(readCount, articleId, callback) {
// 	var sql = 'update article set readCount = ? where articleId = ?';
// 	args = [readCount, articleId];
// 	dbclient.query(sql, args, function(err, res) {
// 		if (err !== null) {
// 			callback(err, null)
// 		} else {
// 			if (!!res && res.affectedRows > 0) {
// 				callback(err, true);
// 			} else {
// 				callback(err, false);
// 			}
// 		}
// 	});
// }

ArticleDao.updateReadCount = function(articleId, callback) {
	new Promise(function(resolve, reject) {
		dbclient.getConnection(function(err, connection) {
			if (err) {
				reject({
					err: err,
					connection: connection
				})
			}
			resolve(connection)
		})
	}).then(function(connection) {
		return new Promise(function(resolve, reject) {
			connection.beginTransaction(function(err) {
				if (err) {
					reject({
						err: err,
						connection: connection
					})
				}
				resolve(connection)
			})
		})
	}).then(function(connection) {
		return new Promise(function(resolve, reject) {
			var sql = 'select readCount from article where articleId=?';
			var args = [articleId];
			connection.query(sql, args, function(err, result) {
				if (err) {
					reject({
						err: err,
						connection: connection
					})
				}
				resolve({
					connection: connection,
					result: result
				})
			})
		})
	}).then(function() {
		var connection = arguments[0].connection;
		var readCount = parseInt(arguments[0].result[0].readCount) + 1;
		return new Promise(function(resolve, reject) {
			var sql = 'update article set readCount = ? where articleId = ?';
			var args = [];
			if (readCount) {
				args = [readCount, articleId];
			}
			connection.query(sql, args, function(err, result) {
				if (err) {
					reject({
						err: err,
						connection: connection
					});
				}
				resolve({
					connection: connection,
					result: result
				});
			})
		})
	}).then(function() {
		var connection = arguments[0].connection
		connection.commit(function(err) {
			if (err) {
				callback && callback(err)
			}
			callback && callback(null);
		})
		connection.release()
	}, function() {
		var err = arguments[0].err;
		var connection = arguments[0].connection;
		connection.rollback(function() {
			callback && callback(err)
		});
		connection.release()
	})
}

ArticleDao.testFunction = function(testContent, callback) {
	var sql = 'insert into test (testContent) values (?)';
	var args = [testContent];
	dbclient.query(sql, args, function(err, res) {
		callback(err, res);
	});

}