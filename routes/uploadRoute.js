/*
 * GET home page.
 */
var Constants = require('../utils/Constants');
var fs = require('fs');

module.exports = function(app) {
	app.post('/upload', function(req, res) {
		for (var i in req.files) {
			if (req.files[i].size == 0) {
				fs.unlinkSync(req.files[i].path);
				res.send({
					error: 0
				})
			} else {
				var name = new Date().getTime() + req.files[i].name;
				var target_path = './public/images/titleImg/' + name;
				fs.renameSync(req.files[i].path, target_path);
				resPath = 'images/titleImg/' + name;
				res.send({
					"error": 0,
					"url": resPath
				});
			}
		}
	})
};

module.exports = function(app) {
	app.post('/md_upload', function(req, res) {
		for (var i in req.files) {
			if (req.files[i].size == 0) {
				fs.unlinkSync(req.files[i].path);
				res.send({
					error: 0
				})
			} else {
				var filename = req.files[i].name;
				var extflag = filename.lastIndexOf(".");
				var ext = filename.slice(extflag, filename.length);
				var name = new Date().getTime() + ext;
				var target_path = './public/images/mdImg/' + name;
				fs.renameSync(req.files[i].path, target_path);
				resPath = 'images/mdImg/' + name;
				res.send(JSON.stringify({
					"error": 0,
					"url": resPath
				}));
			}
		}
	})
};