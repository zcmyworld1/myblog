/*
 * GET home page.
 */

module.exports = function(app) {
	app.get('/', function(req, res) {
		res.sendfile("views/index.html");
	})
	// app.get('/album', function(req, res) {
	// 	res.sendfile('views/album.html');
	// })
	app.get('/photo', function(req, res) {
		res.sendfile('views/photo.html');
	})
	app.get('/music', function(req, res) {
		res.sendfile('views/music.html');
	})
	app.get('/exhibit', function(req, res) {
		res.sendfile('views/exhibit.html');
	})
	app.get('/message', function(req, res) {
		res.sendfile('views/message.html');
	})
	app.get('/aboutme', function(req, res) {
		res.sendfile('views/aboutme.html');
	})
	app.get('/singleArticle', function(req, res) {
		res.sendfile('views/singleArticle.html');
	})
	/*admin*/
	app.get('/articleEdit', function(req, res) {
		res.sendfile('views/admin/articleEdit.html');
	})
	app.get('/articleAdd', function(req, res) {
		res.sendfile('views/admin/articleAdd.html');
	})
	app.get('/photoAdd', function(req, res) {
		res.sendfile('views/admin/photoAdd.html');
	})
	app.get('/mdEditorAdd', function(req, res) {
		res.sendfile('views/admin/mdEditorAdd.html');
	});
	app.get('/mdEditorEdit', function(req, res) {
		res.sendfile('views/admin/mdEditorEdit.html');
	});
};