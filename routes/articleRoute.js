/*
 * GET home page.
 */

var ArticleDao = require('../dao/IndexDao').ArticleDao;
var Constants = require('../utils/Constants');
var fs = require('fs');
// var redisClient = require('../utils/redisClient');
module.exports = function(app) {

	app.post('/article/getTimeHistroy', function(req, res) {
		ArticleDao.getTimeHistroy(function(err, ret) {
			if (err) {
				res.send({
					error: Constants.ERROR.SYS_ERROR
				})
				return;
			}
			res.send({
				error: 0,
				timeHistory: ret
			})
		})
	})
	app.post('/article/getDataCount', function(req, res) {
		ArticleDao.getDataCount(function(err, ret) {
			if (err) {
				res.send({
					error: Constants.ERROR.SYS_ERROR
				})
				return;
			}

			res.send({
				error: 0,
				articleCount: ret
			})
		})
	})
	app.post('/article/addArticle', function(req, res) {
		var content = req.body.content;
		var title = req.body.title;
		var articleType = req.body.articleType;
		var isHaveFile = false;
		for (var i in req.files) {
			if (req.files[i].size == 0) {
				fs.unlinkSync(req.files[i].path);
			} else {
				isHaveFile = true;
				var target_path = './public/images/articleImg/' + req.files[i].name;
				fs.renameSync(req.files[i].path, target_path);
				titleImg = 'images/articleImg/' + req.files[i].name;
				ArticleDao.insertArticle(content, title, titleImg, articleType, 0, function(err, ret) {
					if (err) {
						res.send({
							error: Constants.ERROR.SYS_ERROR
						})
						return;
					}
					// redisClient.set(ret, 0);
					res.send({
						error: 0
					})
				})
			}
		}
		if (isHaveFile) {
			return;
		}
		ArticleDao.insertArticle(content, title, null, articleType, 0, function(err, ret) {
			if (err) {
				res.send({
					error: Constants.ERROR.SYS_ERROR
				})
				return;
			}
			// redisClient.set(ret, 0);
			res.send({
				error: 0
			})
		})
	})

	app.post('/article/updateArticle', function(req, res) {
		var content = req.body.content;
		var title = req.body.title;
		var articleId = req.body.articleId;
		ArticleDao.updateArticle(content, title, articleId, function(err, ret) {
			if (err) {
				res.send({
					error: Constants.ERROR.SYS_ERROR
				})
				return;
			}
			res.send({
				error: 0
			})
		})
	})

	app.post('/article/md_addArticle', function(req, res) {
		var content = req.body.content;
		var md_content = req.body.md_content;
		var title = req.body.title;
		var args = {
			content: content,
			md_content: md_content,
			title: title,
			md_content_history: md_content,
			content_history: content
		}
		ArticleDao.md_insertArticle(args, function(err, ret) {
			if (err) {
				res.send({
					error: Constants.ERROR.SYS_ERROR
				})
				return;
			}
			res.send({
				error: 0
			})
		})
	})

	app.post('/article/md_updateArticle', function(req, res) {
		var content = req.body.content;
		var title = req.body.title;
		var md_content = req.body.md_content;
		var articleId = req.body.articleId;
		ArticleDao.getSingleArticle(articleId, function(err, ret) {
			if (err) {
				res.send({
					error: Constants.ERROR.SYS_ERROR
				});
				return;
			}
			var content_history = ret[0].content;
			var md_content_history = ret[0].md_content;

			ArticleDao.md_updateArticle(content, content_history, md_content, md_content_history,title, articleId, function(err, ret) {
				if (err) {
					res.send({
						error: Constants.ERROR.SYS_ERROR
					})
					return;
				}
				res.send({
					error: 0
				})
			})
		})
	})

	app.post('/article/getArticleList', function(req, res) {
		var currentPage = req.body.currentPage;
		var pageSize = 10;
		var current = (parseInt(currentPage) - 1) * pageSize;
		ArticleDao.getArticleList(current, pageSize, function(err, ret) {
			if (err) {
				res.send({
					error: Constants.ERROR.SYS_ERROR
				})
				return;
			}
			res.send({
				error: 0,
				articleList: ret
			})
		})
	})

	app.post('/article/getSingleArticle', function(req, res) {
		var articleId = req.body.articleId;
		ArticleDao.getSingleArticle(articleId, function(err, ret) {
			if (err) {
				res.send({
					error: Constants.ERROR.SYS_ERROR
				})
				return;
			}
			res.send({
				error: 0,
				article: ret
			})
			ArticleDao.updateReadCount(articleId)
				//忽略这里的报错
				// redisClient.incr(articleId, function(err, ret) {
				// 	ArticleDao.updateReadCount(ret, articleId, function(err, ret) {})
				// });
		})
	})

	app.get('/redis', function(req, res) {
		var key = "key_nod1";
		var value = "value_node";
		// redisClient.set(key, value);
		// console.log(redisClient.get(key));
		redisClient.incr(key, function(err, ret) {
			console.log(ret)
		})
	})
	app.get('/nima', function(req, res) {
		// ArticleDao.updateReadCount2(1,function(err,ret){
		// })
	})
};