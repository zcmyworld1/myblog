/*
 * GET home page.
 */

var Constants = require('../utils/Constants');
var MessageDao = require('../dao/IndexDao').MessageDao;
module.exports = function(app) {


	app.post('/message/messageAdd', function(req, res) {
		var content = req.body.content;
		MessageDao.insertMessage(content, function(err, ret) {
			if (err) {
				res.send({
					error: Constants.ERROR.SYS_ERROR
				})
				return;
			}
			res.send({
				error: 0
			})
		})
	})

	app.post('/message/getMessageList', function(req, res) {
		MessageDao.getMessageList(function(err, ret) {
			if (err) {
				res.send({
					error: Constants.ERROR.SYS_ERROR
				})
				return;
			}
			res.send({
				error: 0,
				messageList: ret
			})
		})
	})

};